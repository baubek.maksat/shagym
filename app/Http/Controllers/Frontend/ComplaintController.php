<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ComplaintView;
use App\Models\Settlement;
use App\Models\Negative;
use App\Models\Region;

class ComplaintController extends Controller
{
    public function show(ComplaintView $complaint_view)
    {
    	$complaint_view = ComplaintView::query()
    		->with('translation')
    		->with('negatives')
    		->where('id', $complaint_view->id)
    		->first();

    	$regions = Region::query()
    		->with('translation')
    		->get();

    	$settlements = Settlement::query()
    		->with('translation')
    		->get();

        return view('form')->with([
			'view_id' => $complaint_view->id, 
			'regions' => $regions, 
			'settlements' => $settlements, 
			'complaint_view' => $complaint_view
		])->render();
    }
}
