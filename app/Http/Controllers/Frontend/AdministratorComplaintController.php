<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ComplaintView;
use App\Models\Settlement;
use App\Models\Complaint;
use App\Models\Negative;
use App\Models\Region;
use App\Models\Client;

use Validator;

class AdministratorComplaintController extends Controller
{
    public function index(Request $request)
    {
    	$complaints = Complaint::query()
            ->with('region')
            ->with('settlement')
            ->with('view')
            ->with('client')
    		->with('negatives')
            ->where('status_id', '!=' , 3);

        if ($request->has('region_id') && $request->region_id != 0) {
            $complaints->where('region_id', $request->region_id);
        }

        if ($request->has('client_id') && $request->client_id != 0) {
            $complaints->where('client_id', $request->client_id);
        }

        if ($request->has('settlement_id') && $request->settlement_id != 0) {
            $complaints->where('settlement_id', $request->settlement_id);
        }

        if ($request->has('view_id') && $request->view_id != 0) {
            $complaints->where('view_id', $request->view_id);
        }
		
		if ($request->has('negative_id') && $request->negative_id != 0) {
            $complaints->whereHas('negatives', function ($query) use ($request){
				$query->where('id', '=', $request->negative_id);
			});
        }

        if ($request->has('score') && $request->score) {
            $complaints->where('score', $request->score);
        }

        if ($request->has('created_at') && $request->created_at) {
            $complaints->where('created_at', 'like', substr($request->created_at, 0, 10).'%');
        }

        $complaints_score['sum'] = round($complaints->sum('score'), 3);
        $complaints_score['middle'] = round($complaints->avg('score'), 3);

    	$complaints = $complaints->paginate();

        $complaint_views = ComplaintView::query()
            ->with('translation')
            ->get();

    	$regions = Region::query()
    		->with('translation')
    		->get();

    	$settlements = Settlement::query()
    		->with('translation')
    		->get();

        $negatives = Negative::query()
            ->with('translation')
            ->get();

        $complaints_filter = Complaint::query()
            ->get();

        return view('complaint.index', [
            'complaints' => $complaints,
            'complaint_views' => $complaint_views,
            'regions' => $regions,
            'settlements' => $settlements,
            'negatives' => $negatives,
            'request' => $request,
            'complaints_score' => $complaints_score,
            'complaints_filter' => $complaints_filter,
        ]);
    }

    public function edit(Complaint $complaint)
    {
        $complaint = Complaint::query()
            ->with('region')
            ->with('settlement')
            ->with('view')
            ->with('negatives')
            ->where('id', $complaint->id)
            ->first();

        $complaint_views = ComplaintView::query()
            ->with('translation')
            ->get();

        $regions = Region::query()
            ->with('translation')
            ->get();

        $сlient = Client::query()
            ->where('id', $complaint->client_id)
            ->first();

        $settlements = Settlement::query()
            ->with('translation')
            ->get();

        $negatives = Negative::query()
            ->with('translation')
            ->get();

        return view('complaint.edit', [
            'complaint' => $complaint,
            'complaint_views' => $complaint_views,
            'regions' => $regions,
            'сlient' => $сlient,
            'settlements' => $settlements,
            'negatives' => $negatives
        ]);
    }

    public function show(Complaint $complaint)
    {
        $complaint = Complaint::query()
            ->with('region')
            ->with('settlement')
            ->with('view')
            ->with('negatives')
            ->where('id', $complaint->id)
            ->first();

        $сlient = Client::query()
            ->where('id', $complaint->client_id)
            ->first();

        return view('complaint.show', [
            'complaint' => $complaint,
            'сlient' => $сlient,
        ]);
    }
	
	public function exportCsv(Request $request)
	{
		
	$fileName = 'shagym.kz_'.date('Y_m_d').'.csv';
	
	
    $complaints = Complaint::query()
            ->with('region')
            ->with('settlement')
            ->with('view')
    		->with('negatives');

        if ($request->has('region_id') && $request->region_id != 0) {
            $complaints->where('region_id', $request->region_id);
        }

        if ($request->has('settlement_id') && $request->settlement_id != 0) {
            $complaints->where('settlement_id', $request->settlement_id);
        }

        if ($request->has('view_id') && $request->view_id != 0) {
            $complaints->where('view_id', $request->view_id);
        }

        if ($request->has('score') && $request->score) {
            $complaints->where('score', $request->score);
        }

        if ($request->has('created_at') && $request->created_at) {
            $complaints->where('created_at', 'like', substr($request->created_at, 0, 10).'%');
        }
		
		$complaints = $complaints->get();
	

     $headers = array(
		 "Content-Encoding" => "UTF-8",
        "Content-type"        => "text/csv; charset=UTF-8",
        "Content-Disposition" => "attachment; filename=$fileName",
       "Pragma"              => "no-cache",
       "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
       "Expires"             => "0"
      );
  
      $columns = array('ID', 'На что жалоба', 'Регион', 'Населенный пункт', 'Дата подачи жалобы', 'Что не понравилось', 'Баллы');

      $callback = function() use($complaints, $columns) {
        $file = fopen('php://output', 'w');
        fprintf($file, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($file, $columns, ';');

        foreach ($complaints as $complaint) {
			
			$negatives = [];
			
			 foreach($complaint->negatives as $negative) {
                 $negatives[] = $negative->translation->name;
			}
			
          fputcsv($file, array($complaint->id, $complaint->view->translation->name, $complaint->region->translation->name, $complaint->settlement->translation->name, substr($complaint->created_at, 0, 10), implode(', ', $negatives), $complaint->score ?: 0), ';');
			
			$negatives = [];
        }
	echo "\xEF\xBB\xBF"; // UTF-8 BOM
	
        fclose($file);
      };

     return response()->stream($callback, 200, $headers);
	}
}
