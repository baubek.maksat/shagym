<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ComplaintView;

class HomeController extends Controller
{
    public function index()
    {
    	$complaint_views = ComplaintView::query()
    		->with('translation')
    		->get();

        return view('home');
    }
}
