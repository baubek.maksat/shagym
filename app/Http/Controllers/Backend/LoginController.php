<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Models\ComplaintView;
use App\Models\Complaint;
use App\Models\Client;

use Validator;

use DB;

class LoginController extends Controller
{
	public function login(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'email' => 'required|string|max:255',
            'password' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages()
            ], 200);
        }

        if (!auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json([
                'code' => 401
            ], 200);
        }

        return response()->json([
            'code' => 200
        ], 200);
    }

    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();

        //return redirect('frontend.home');
        return true;
    }
}
