<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Models\ComplaintView;
use App\Models\Complaint;
use App\Models\Client;

use Validator;

use DB;

class ComplaintController extends Controller
{
	public function index()
	{
		$complaints = ComplaintPhoto::query()
			->paginate();

		foreach ($complaints as $complaint) {
			dd($complaint);
		}
	}

    public function store(Request $request)
    {

    	$validator = Validator::make($request->all(), [
            'view_id' => 'required|exists:complaint_views,id',
            'name' => 'required|string|max:255',
            'subname' => 'required_if:view_id,2|string|max:255',
            'region_id' => 'required|exists:regions,id',
            'settlement_id' => 'exists:settlements,id',
            'date_start' => 'date',
            'date_end' => 'date',
            'negatives' => 'array',
            'negatives.*' => 'exists:negatives,id',
            'comment' => Rule::requiredIf(function() use($request) {
            	foreach ($request->negatives as $negative) {
            		if ($negative == 7) {
            			return true;
            		}
            	}
            }),
			'photos' => 'array',
            'photos.*' => 'mimes:jpeg,jpg,png',
            'client_name' => 'required',
            'client_email' => 'required|email|max:255',
            'client_phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages()
            ], 200);
        }

        $result = DB::transaction(function() use ($request) {
            $client = Client::query()
                ->where('name', $request->client_name)
                ->where('email', $request->client_email)
                ->where('phone', $request->client_phone)
                ->first();

            if (!$client) {
                $client = Client::create([
                    'name' => $request->client_name,
                    'email' => $request->client_email,
                    'phone' => $request->client_phone
                ]);
            }

            $complaint = Complaint::create([
                'view_id' => $request->view_id,
                'name' => $request->name,
                'subname' => $request->subname,
                'client_id' => $client->id,
                'region_id' => $request->region_id,
                'settlement_id' => $request->settlement_id,
                'date_start' => $request->has('date_start') ? $request->date_start : null,
                'date_end' => $request->has('date_end') ? $request->date_end : null,
                'comment' => $request->comment,
				'comment_text' => $request->comment_text,
                'status_id' => 1
            ]);

            if ($request->has('negatives')) {
            	foreach ($request->negatives as $negative) {
            		$complaint_view = ComplaintView::query()
		            	->where('id', $negative)
		            	->first();

		            if ($complaint_view) {
		            	$complaint->negatives()->attach($complaint_view);
		            }
            	}
            }

            if ($request->has('photos')) {
            	foreach ($request->photos as $photo) {
            		$src = Storage::putFileAs('uploads/'.date('y/m/d/h/i/s'), $photo, rand(1000, 9999).'.'.$photo->extension());

            		$complaint->photos()->create([
		                'src' => 'storage/'.$src
		            ]);
            	}
            }
        });

        return response()->json([
            'message' => 'Created'
        ], 201);
    }

    public function update(Request $request, Complaint $complaint)
    {
        $validator = Validator::make($request->all(), [
            'view_id' => 'required|exists:complaint_views,id',
            'name' => 'required|string|max:255',
            'subname' => 'required_if:view_id,2|string|max:255',
            'region_id' => 'required|exists:regions,id',
            'settlement_id' => 'exists:settlements,id',
            'date_start' => 'date',
            'date_end' => 'date',
            'negatives' => 'array',
            'negatives.*' => 'exists:negatives,id',
            'comment' => Rule::requiredIf(function() use($request) {
                foreach ($request->negatives as $negative) {
                    if ($negative == 7) {
                        return true;
                    }
                }
            }),
            'photos' => 'array',
            'photos.*' => 'mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages()
            ], 200);
        }

        $result = DB::transaction(function() use ($request, $complaint) {
            $complaint->update([
                'view_id' => $request->view_id,
                'name' => $request->name,
                'subname' => $request->subname,
                'region_id' => $request->region_id,
                'settlement_id' => $request->settlement_id,
                'date_start' => $request->date_start,
                'date_end' => $request->date_end,
                'comment' => $request->comment,
                'score' => $request->score,
                'status_id' => 1
            ]);

            if ($request->has('negatives')) {
                $complaint->negatives()->detach();

                foreach ($request->negatives as $negative) {
                    $complaint_view = ComplaintView::query()
                        ->where('id', $negative)
                        ->first();

                    if ($complaint_view) {
                        $complaint->negatives()->attach($complaint_view);
                    }
                }
            }

            /*

            if ($request->has('photos')) {
                foreach ($request->photos as $photo) {
                    $src = Storage::putFileAs('uploads/'.date('y/m/d/h/i/s'), $photo, rand(1000, 9999).'.'.$photo->extension());

                    $complaint->photos()->create([
                        'src' => 'storage/'.$src
                    ]);
                }
            }
            */
        });

        return response()->json([
            'code' => 200,
            'message' => 'Updated'
        ], 200);
    } 

    public function destroy(Complaint $complaint)
    {
        // $result = Complaint::destroy($id);
        
        $complaint->update(array('status_id' => 3));


        return redirect()->route('frontend.administrator.complaints.index')->with(['success' => "Запись удалена"]);
    }
    
}
