<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
    	'slug'
    ];

    public function translation()
    {
        return $this->hasOne(RoleLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }
}
