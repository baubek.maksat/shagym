<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintView extends Model
{
    use HasFactory;

    protected $table = 'complaint_views';

    protected $fillable = [
    	'id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(ComplaintViewLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }

    public function negatives()
    {
        return $this->belongsToMany(Negative::class, 'complaint_views_negatives');
    }

    public function hasNegative($negative) {
        if ($this->negatives->contains('id', $negative)) {
            return true;
        } else {
            return false;
        }
    }
}
