<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintStatusLang extends Model
{
    use HasFactory;

    protected $table = 'complaint_status_langs';

    protected $fillable = [
    	'id',
    	'lang',
    	'name'
    ];
}
