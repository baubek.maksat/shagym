<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintStatus extends Model
{
    use HasFactory;

    protected $table = 'complaint_statuses';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(ComplaintStatuslang::class, 'id', 'id')->where('lang', app()->getLocale());
    }
}
