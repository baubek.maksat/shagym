<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleLang extends Model
{
    use HasFactory;

    protected $table = 'role_langs';

    protected $fillable = [
    	'slug',
    	'name'
    ];
}
