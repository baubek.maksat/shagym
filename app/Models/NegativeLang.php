<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NegativeLang extends Model
{
    use HasFactory;

    protected $table = 'negative_langs';

    protected $fillable = [
    	'id',
        'lang',
        'name'
    ];
}
