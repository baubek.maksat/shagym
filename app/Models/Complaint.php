<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use HasFactory;

    protected $table = 'complaints';

    protected $fillable = [
    	'id',
        'view_id',
        'client_id',
        'region_id',
        'settlement_id',
        'name',
        'date_start',
        'date_end',
        'comment',
        'status_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function view()
    {
        return $this->hasOne(ComplaintView::class, 'id', 'view_id');
    }

    public function status()
    {
        return $this->hasOne(ComplaintStatus::class, 'id', 'status_id');
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'region_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function settlement()
    {
        return $this->hasOne(Settlement::class, 'id', 'settlement_id');
    }

    public function photos()
    {
        return $this->hasMany(ComplaintPhoto::class, 'id', 'id');
    }

    public function negatives()
    {
        return $this->belongsToMany(Negative::class, 'complaints_negatives');
    }
}
