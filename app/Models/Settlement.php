<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{
    use HasFactory;

    protected $table = 'settlements';

    protected $fillable = [
    	'id',
    	'region_id'
    ];

    public function translation()
    {
        return $this->hasOne(SettlementLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }

    public function region()
    {
        return $this->hasOne(Region::class, 'id', 'region_id');
    }
}
