<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory;

    protected $table = 'regions';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(RegionLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }
}
