<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Negative extends Model
{
    use HasFactory;

    protected $table = 'negatives';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(NegativeLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }

    public function complaintViews()
    {
        return $this->belongsToMany(ComplaintView::class, 'complaint_views_negatives');
    }

    public function hasComplaintView($complaint_view) {
        if ($this->complaintViews->contains('id', $complaint_view)) {
            return true;
        } else {
            return false;
        }
    }
}
