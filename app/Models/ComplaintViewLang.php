<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintViewLang extends Model
{
    use HasFactory;

    protected $table = 'complaint_view_langs';

    protected $fillable = [
    	'id',
    	'lang',
    	'name'
    ];
}
