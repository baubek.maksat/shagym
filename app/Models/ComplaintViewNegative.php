<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintViewNegative extends Model
{
    use HasFactory;

    protected $table = 'complaint_views_negatives';

    protected $fillable = [
    	'complaint_view_id',
    	'negative_id'
    ];
}
