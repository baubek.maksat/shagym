<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Переключение языков
Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl();; //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

//    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], config("app.locales"))) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    array_splice($segments, 1, 0, $lang);

    //формируем полный URL
    $url = Request::root().implode("/", $segments);

    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');


//	Frontend

Route::group(['namespace' => 'Frontend'], function(){

	Route::get('login', 'LoginController@in')->name('frontend.login');

	Route::group([
        'prefix' => LocalizationService::locale(),
        'middleware' => 'setLocale'
    ], function(){

		Route::get('', 'HomeController@index')->name('frontend.home');
		Route::get('complaint-views/{complaint_view}', 'ComplaintController@show')->name('frontend.complaints.show')->where([
			'complaint_view' => '[0-9]+'
		]);
		
	});
	
	Route::group(['middleware' => 'auth'], function() {
		Route::get('administrator/complaints', 'AdministratorComplaintController@index')->name('frontend.administrator.complaints.index');
		Route::get('administrator/complaints/export/csv', 'AdministratorComplaintController@exportCsv')->name('frontend.administrator.complaints.export.csv');
		Route::get('administrator/complaints/{complaint}', 'AdministratorComplaintController@show')->name('frontend.administrator.complaints.show')->where([
			'complaint' => '[0-9]+'
		]);
		Route::get('administrator/complaints/{complaint}/edit', 'AdministratorComplaintController@edit')->name('frontend.administrator.complaints.edit')->where([
			'complaint' => '[0-9]+'
		]);
	});

});


//	Backend

Route::group(['namespace' => 'Backend'], function(){
	Route::post('login', 'LoginController@login')->name('backend.login');

	Route::post('complaints', 'ComplaintController@store')->name('backend.complaints.store');

	Route::group(['middleware' => 'auth'], function() {
		Route::post('logout', 'LoginController@logout')->name('backend.logout');

		Route::put('complaints/{complaint}', 'ComplaintController@update')->name('backend.complaints.update');
		// Route::delete('complaints/{complaint}', 'ComplaintController@destroy')->name('backend.complaints.destroy');
		Route::get('complaints/{complaint}/destroy', 'ComplaintController@destroy')->name('backend.complaints.destroy');
	});
});

Route::get('/complaint', function(){
	return view('complaint.index');
});
Route::get('/complaint/{id}', function(){
	return view('complaint.show');
});
Route::get('/complaint/{id}/edit', function(){
	return view('complaint.edit');
});

use App\Models\Complaint;
use App\Models\SettlementLang;
use App\Models\ComplaintViewLang;
use App\Models\RegionLang;
use App\Models\NegativeLang;

Route::get('/test', function(){
	$row = 1;
	
	if (($handle = fopen(asset('shagym.kz_2021_07_08.csv'), "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
				
			$columns = explode(';', $data[0]);
			
			if($row > 1) {
				if (count($columns) == 6) {
					echo "<pre>";
					print_r($columns);
					echo "</pre>";
				}
			}
			
			$row++;
		}
		
		fclose($handle);
	}
});
