$('#date_range').datepicker({
    range: 'period', // режим - выбор периода
    numberOfMonths: 2,
    // dateFormat: "dd.mm.yy",
    dateFormat: "yy-mm-dd",
    showOn: "button",
    buttonImage: "/img/calendar.svg",

    onSelect: function (dateText, inst, extensionRange) {
        // extensionRange - объект расширения
        $('#date_range').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
        $('#date_start').val(extensionRange.startDateText);
        $('#date_end').val(extensionRange.endDateText);

    }
});

$.datepicker.setDefaults($.datepicker.regional["ru"]);


$(".biznes__title").click(function(e){
    $(this).parents(".header__right--biznes").toggleClass("header__right--active");
});
$(".header__language--title").click(function(e){
    $(this).parents(".header__language").toggleClass("header__language--active");
});

$(document).on( "click", function( e ) {
    if (!$(".header__right--biznes").is(e.target) && $(".header__right--biznes").has(e.target).length === 0) {
        $(".header__right--biznes").removeClass('header__right--active');
    }
    if (!$("header__language").is(e.target) && $(".header__language").has(e.target).length === 0) {
        $(".header__language").removeClass('header__language--active');
    }
});





$('input#comment-files').on('change', function () {
    showFileNames();
    return false;
});
function showFileNames() {
    var files = $("input:file[name='photos\[\]']").prop("files");

    var attachedFilesList = $('ul#attached-fiels-list');
    attachedFilesList.html('');
    if (files.length > 0) {

        for (var i = 0; i < files.length; i++) {
            attachedFilesList.append('<li>' + files[i].name + '</li>');
        }
    } else {
        attachedFilesList.html('нет прикреплённых файлов');
    }
}





jQuery.fn.chained = function (parent_selector, options) {
    return this.each(function () {
        var self = this;                                        /* Save this to self because this changes when scope changes. */
        var backup = jQuery(self).clone();
        jQuery(parent_selector).each(function () {               /* Handles maximum two parents now. */
            jQuery(this).bind("change", function () {
                jQuery(self).html(backup.html());
                var selected = "";                                      /* If multiple parents build classname like foo\bar. */
                jQuery(parent_selector).each(function () { selected += "\\" + jQuery(":selected", this).val(); });
                selected = selected.substr(1);                          /* Also check for first parent without subclassing. */ /* TODO: This should be dynamic and check for each parent */ /* without subclassing. */
                var first = jQuery(parent_selector).first();
                var selected_first = jQuery(":selected", first).data("region");
                jQuery("option", self).each(function () {
                    if (jQuery(this).data("city") != selected_first) { jQuery(this).remove(); }
                });
                // if (1 == jQuery("option", self).size() && jQuery(self).val() === "") { jQuery(self).attr("disabled", "disabled"); }  /* If we have only the default value disable select. */
                // else { jQuery(self).removeAttr("disabled"); }
                jQuery(self).trigger("change");
            });
            if (!jQuery("option:selected", this).length) { jQuery("option", this).first().attr("selected", "selected"); }       /* Force IE to see something selected on first page load, */ /* unless something is already selected */
            jQuery(this).trigger("change");                        /* Force updating the children. */
        });
    });
};
jQuery.fn.chainedTo = jQuery.fn.chained;               /* Alias for those who like to use more English like syntax. */
jQuery(document).ready(function () { jQuery("#city").chained("#region"); });


$('input[type="checkbox"][value="7"]').click(function(e){
    if($(this)[0].checked){
        $(this).parents(".checkbox__input").siblings('.liked__other').show();
    }else{
        $(this).parents(".checkbox__input").siblings('.liked__other').hide();
    }
});

var spage = '';
$(".form__next, .input__button-click").click(function(){
    var listitem = $(".form__listitem--active");
    var error_par = false;
    var forminput = listitem.find('input[type=\'text\'], input[type=\'hidden\'], input[type=\'radio\']:checked, input[type=\'checkbox\']:checked, select, textarea');
    forminput.each(function(){
        if($(this).attr('required')){
            if($(this).val() != ''){
                $(this).removeClass('required__error');
                $(this).siblings('.input-required').hide();
            } else {
                $(this).addClass('required__error');
                $(this).siblings('.input-required').show();
                error_par = true;
            }
        }
    });

    if(!error_par){
        spage = listitem.data('spage');
        $(".form__block--next").hide();
        $(".form__listitem").removeClass("form__listitem--active");
        $(".form__listitem--s9").addClass("form__listitem--active");
    }

});
$(".form__prev a").click(function(e){
    if(spage &&  spage != 's9'){
        e.preventDefault();
        $(".form__block--next").show();
        $(".form__listitem").removeClass("form__listitem--active");
        $(".form__listitem--"+spage).addClass("form__listitem--active");
        spage = '';
    }
});


$(function() {
    // $('input[name="client_phone"]').mask('+7(999)-999-99-99');
    // $('input[name="client_email"]').mask("*{3,20}@*{3,20}.*{2,7}");
    $('input[name="client_phone"]').inputmask('+7(999)-999-99-99');
    // $('input[name="client_email"]').inputmask("email");
});

$(".header-toggle").click(function(e){
    $("body").toggleClass("header--menu-open");
});


jQuery("#form__list").submit(function () {
    var formNm2 = jQuery('#form__list');
    var formData = new FormData(this);

    if(formData.get('date_start') == '' && formData.get('date_end') == ''){
        formData.delete('date_start');
        formData.delete('date_end');
    }

    var captcha = grecaptcha.getResponse();
    // Такую форму не будем отправлять на сервер.
    if (!captcha.length) {
        $('#recaptchaError').show();
    } else {
        $('#recaptchaError').hide();

        jQuery.ajax({
            type: "POST",
            url: "/complaints",
            // data: formNm2.serialize(),
            cache: false, 
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                console.log(data);
                $("#success").addClass("md-show")
                jQuery(this).find("input").val("");
                jQuery(formNm2).trigger("reset");
            },
            error: function (jqXHR, text, error) {
                // jQuery(formNm2).html(error);
                console.log(error);
            }
        });

    }

    return false;
});

jQuery("#logint_form").submit(function () {
    var formLogin = jQuery('#logint_form');
    jQuery.ajax({
        type: "POST",
        url: "/login",
        data: formLogin.serialize(),
        success: function (data) {
            if(data.code == 200){
                formLogin.find(".input-linck").removeClass('required__error');
                formLogin.find(".input-linck").siblings('.input-required').hide();
                $(location).attr('href','/administrator/complaints');
            } else {
                formLogin.find(".input-linck").addClass('required__error');
                formLogin.find(".input-linck").siblings('.input-required').show();
            }
        },
        error: function (jqXHR, text, error) {
            // jQuery(formNm2).html(error);
            console.log(error);
        }
    });
    return false;
});
