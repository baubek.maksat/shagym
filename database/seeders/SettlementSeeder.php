<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Settlement;

class SettlementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settlements = [
            [
            	'region_id' => 1,
                'name_ru' => 'Нур-Султан',
            ],[
                'region_id' => 2,
                'name_ru' => 'Алматы',
            ],[
                'region_id' => 3,
                'name_ru' => 'Шымкент',
            ],[	
				'region_id' => '4',
				'name_ru' => 'Кокшетау',
			],[	
				'region_id' => '4',
				'name_ru' => 'Боровое (Щучинск)',
			],[	
				'region_id' => '4',
				'name_ru' => 'Зеренда',
			],[	
				'region_id' => '4',
				'name_ru' => 'Коргалжын',
			],[	
				'region_id' => '4',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '5',
				'name_ru' => 'Актюбинск',
			],[	
				'region_id' => '5',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '6',
				'name_ru' => 'Талдыкорган',
			],[	
				'region_id' => '6',
				'name_ru' => 'Алаколь',
			],[	
				'region_id' => '6',
				'name_ru' => 'Капчагай',
			],[	
				'region_id' => '6',
				'name_ru' => 'Чунджа',
			],[	
				'region_id' => '6',
				'name_ru' => 'Озера Колсай и Каинды',
			],[	
				'region_id' => '6',
				'name_ru' => 'Балхаш',
			],[	
				'region_id' => '6',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '7',
				'name_ru' => 'Атырау',
			],[	
				'region_id' => '7',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '8',
				'name_ru' => 'Усть-Каменогорск',
			],[	
				'region_id' => '8',
				'name_ru' => 'Семипалатинск',
			],[	
				'region_id' => '8',
				'name_ru' => 'Озеро Алаколь',
			],[	
				'region_id' => '8',
				'name_ru' => 'Бухтарминское водохранилище',
			],[	
				'region_id' => '8',
				'name_ru' => 'Сибинские озера',
			],[	
				'region_id' => '8',
				'name_ru' => 'Катон-Карагай',
			],[	
				'region_id' => '8',
				'name_ru' => 'Риддер',
			],[	
				'region_id' => '8',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '9',
				'name_ru' => 'Тараз',
			],[	
				'region_id' => '9',
				'name_ru' => 'Мерке',
			],[	
				'region_id' => '9',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '10',
				'name_ru' => 'Уральск',
			],[	
				'region_id' => '10',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '11',
				'name_ru' => 'Караганда',
			],[	
				'region_id' => '11',
				'name_ru' => 'Жезказган',
			],[	
				'region_id' => '11',
				'name_ru' => 'Балхаш',
			],[	
				'region_id' => '11',
				'name_ru' => 'Улытау',
			],[	
				'region_id' => '11',
				'name_ru' => 'Каркаралинск',
			],[	
				'region_id' => '11',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '12',
				'name_ru' => 'Костанай',
			],[	
				'region_id' => '12',
				'name_ru' => 'Другое',
			],[
				'region_id' => '13',
				'name_ru' => 'Кызылорда',
			],[	
				'region_id' => '13',
				'name_ru' => 'Байконур',
			],[	
				'region_id' => '13',
				'name_ru' => 'Жанакорган',
			],[	
				'region_id' => '13',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '14',
				'name_ru' => 'Актау',
			],[	
				'region_id' => '14',
				'name_ru' => 'Побережье Каспийского моря',
			],[	
				'region_id' => '14',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '15',
				'name_ru' => 'Павлодар',
			],[	
				'region_id' => '15',
				'name_ru' => 'Баянаул',
			],[	
				'region_id' => '15',
				'name_ru' => 'Мойылды',
			],[	
				'region_id' => '15',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '16',
				'name_ru' => 'Петропавловск',
			],[	
				'region_id' => '16',
				'name_ru' => 'Имантау-Шалкар',
			],[	
				'region_id' => '16',
				'name_ru' => 'Другое',
			],[	
				'region_id' => '17',
				'name_ru' => 'Туркестан',
			],[	
				'region_id' => '17',
				'name_ru' => 'Сарыагаш',
			],[
				'region_id' => '17',
				'name_ru' => 'Другое'
			]
        ];

        for ($i = 0; $i < count($settlements); $i++) { 
           $settlement = Settlement::create([
                'region_id' => $settlements[$i]['region_id']
            ]);

                $settlement->translation()->create([
                	'lang' => 'ru',
                	'name' => $settlements[$i]['name_ru']
                ]);
        }
    }
}
