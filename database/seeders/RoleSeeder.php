<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'slug' => 'administrator',
                'name_kz' => 'Әкімшісі',
                'name_ru' => 'Админстратор',
                'name_en' => 'Administrator'
            ]
        ];

        for ($i = 0; $i < count($roles); $i++) { 
            $role = Role::query()
                ->where('id', $roles[$i]['id'])
                ->first();

            if ($role == null) {
                $role = Role::create([
                	'id' => $roles[$i]['id'],
                	'slug' => $roles[$i]['slug']
                ]);

                $role->translation()->create([
                	'lang' => 'kz',
                	'name' => $roles[$i]['name_kz']
                ]);

                $role->translation()->create([
                	'lang' => 'ru',
                	'name' => $roles[$i]['name_ru']
                ]);

                $role->translation()->create([
                	'lang' => 'en',
                	'name' => $roles[$i]['name_en']
                ]);
            }
        }
    }
}
