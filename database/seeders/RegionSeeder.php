<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'id' => 1,
                'name_ru' => 'Нур-Султан',
            ],[
                'id' => 2,
                'name_ru' => 'Алматы',
            ],[
                'id' => 3,
                'name_ru' => 'Шымкент',
            ],[
                'id' => 4,
                'name_ru' => 'Акмолинская область',
            ],[
                'id' => 5,
                'name_ru' => 'Актюбинская область',
            ],[
                'id' => 6,
                'name_ru' => 'Алматинская область',
            ],[
                'id' => 7,
                'name_ru' => 'Атырауская область',
            ],[
                'id' => 8,
                'name_ru' => 'Восточно-Казахстанская область',
            ],[
                'id' => 9,
                'name_ru' => 'Жамбылская область',
            ],[
                'id' => 10,
                'name_ru' => 'Западно-Казахстанская область',
            ],[
                'id' => 11,
                'name_ru' => 'Карагандинская область',
            ],[
                'id' => 12,
                'name_ru' => 'Костанайская область',
            ],[
                'id' => 13,
                'name_ru' => 'Кызылординская область',
            ],[
                'id' => 14,
                'name_ru' => 'Мангистауская область',
            ],[
                'id' => 15,
                'name_ru' => 'Павлодарская область',
            ],[
                'id' => 16,
                'name_ru' => 'Северо-Казахстанская область',
            ],[
                'id' => 17,
                'name_ru' => 'Туркестанская область',
            ]
        ];

        for ($i = 0; $i < count($regions); $i++) { 
            $region = Region::query()
                ->where('id', $regions[$i]['id'])
                ->first();

            if ($region == null) {
                $region = Region::create([
                	'id' => $regions[$i]['id']
                ]);

                $region->translation()->create([
                	'lang' => 'ru',
                	'name' => $regions[$i]['name_ru']
                ]);
            }
        }
    }
}
