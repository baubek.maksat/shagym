<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ComplaintView;

class ComplainViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $complain_views = [
            [
                'id' => 1,
                'name_ru' => 'Отель/гостиница/база отдыха/санаторий/другое место'
            ],[
                'id' => 2,
                'name_ru' => 'Турфирма (туроператор, турагент)'
            ],[
            	'id' => 3,
                'name_ru' => 'Гид'
            ],[
                'id' => 4,
                'name_ru' => 'Транспортная компания'
            ],[
                'id' => 5,
                'name_ru' => 'Ресторан/кафе/другой пункт питания'
            ],[
                'id' => 6,
                'name_ru' => 'Услуги развлечения'
            ],[
                'id' => 7,
                'name_ru' => 'Другое'
            ]
        ];

        if (count($complain_views) > 0) {
            for ($i = 0; $i < count($complain_views); $i++) { 
                $complain_view = ComplaintView::query()
                    ->where('id', $complain_views[$i]['id'])
                    ->first();

                if ($complain_view == null) {
                    $complain_view = ComplaintView::create([
                    	'id' => $complain_views[$i]['id']
                    ]);

	                $complain_view->translation()->create([
	                	'lang' => 'ru',
	                	'name' => $complain_views[$i]['name_ru']
	                ]);
                }
            }
        }
    }
}
