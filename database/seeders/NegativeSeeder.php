<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ComplaintViewNegative;
use App\Models\ComplaintView;
use App\Models\Negative;

class NegativeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $negatives = [
            [
                'id' => 1,
                'name_ru' => 'Грязно в номерах',
                'complaint_views' => [
                	1
                ]
            ],[
                'id' => 2,
                'name_ru' => 'Грубый персонал',
                'complaint_views' => [
                	1, 2, 3, 4, 5, 6, 7
                ]
            ],[
                'id' => 3,
                'name_ru' => 'Мусор на территории',
                'complaint_views' => [
                	1, 5, 6, 7
                ]
            ],[
                'id' => 4,
                'name_ru' => 'Номера не соответствуют описанию и фотографиям',
                'complaint_views' => [
                	1
                ]
            ],[
                'id' => 5,
                'name_ru' => 'Проблемы с бронированием (например, овербукинг)',
                'complaint_views' => [
                	1
                ]
            ],[
                'id' => 6,
                'name_ru' => 'Проблемы с сантехникой или подачей воды',
                'complaint_views' => [
                	1
                ]
            ],[
                'id' => 7,
                'name_ru' => 'Другое',
                'complaint_views' => [
                	1, 2, 3, 4, 5, 6, 7
                ]
            ],[
                'id' => 8,
                'name_ru' => 'Турфирма не отвечает на запросы',
                'complaint_views' => [
                	2
                ]
            ],[
                'id' => 9,
                'name_ru' => 'Тур оказался не такой, как мне описывали',
                'complaint_views' => [
                	2, 3, 4
                ]
            ],[
                'id' => 10,
                'name_ru' => 'Были дополнительные расходы, о которых не сообщали ранее',
                'complaint_views' => [
                	2, 3
                ]
            ],[
                'id' => 11,
                'name_ru' => 'Не была обеспечена безопасность туриста',
                'complaint_views' => [
                	2, 3, 4, 6, 7
                ]
            ],[
                'id' => 12,
                'name_ru' => 'Неинтересный рассказчик',
                'complaint_views' => [
                	3, 4
                ]
            ],[
                'id' => 13,
                'name_ru' => 'В транспорте грязно',
                'complaint_views' => [
                	4
                ]
            ],[
                'id' => 14,
                'name_ru' => 'Грязно',
                'complaint_views' => [
                	5, 6, 7
                ]
            ],[
                'id' => 15,
                'name_ru' => 'Еда невкусная',
                'complaint_views' => [
                	5
                ]
            ],[
                'id' => 16,
                'name_ru' => 'В туалете заведения грязно/плохой запах',
                'complaint_views' => [
                	5, 6, 7
                ]
            ],[
                'id' => 17,
                'name_ru' => 'Транспорт не соответствует описанию и фотографиям',
                'complaint_views' => [
                	4
                ]
            ]
        ];

        if (count($negatives) > 0) {
            for ($i = 0; $i < count($negatives); $i++) { 
                $negative = Negative::query()
                    ->where('id', $negatives[$i]['id'])
                    ->first();

                if ($negative == null) {
                    $negative = Negative::create([
                    	'id' => $negatives[$i]['id']
                    ]);

                    $negative->translation()->create([
	                	'lang' => 'ru',
	                	'name' => $negatives[$i]['name_ru']
	                ]);
                    
                    for ($j = 0; $j < count($negatives[$i]['complaint_views']); $j++) { 
                        $complaint_view = ComplaintView::query()
                            ->where('id', $negatives[$i]['complaint_views'][$j])
                            ->first();

                        if (!$negative->hasComplaintView($negatives[$i]['complaint_views'][$j])) {
                            $negative->complaintViews()->attach($complaint_view);
                        }
	                }
                } else {
                    for ($j = 0; $j < count($negatives[$i]['complaint_views']); $j++) { 
                        $complaint_view = ComplaintView::query()
                            ->where('id', $negatives[$i]['complaint_views'][$j])
                            ->first();

                        if (!$negative->hasComplaintView($negatives[$i]['complaint_views'][$j])) {
                            $negative->complaintViews()->attach($complaint_view);
                        }
                    }
                }
            }
        }
    }
}
