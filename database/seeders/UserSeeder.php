<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'name' => 'Жизель Бундхен',
                'email' => 'gisele.bundchen@gmail.com',
                'password' => bcrypt('gisele1980')
            ]
        ];

        for ($i = 0; $i < count($users); $i++) { 
            $user = User::query()
                ->where('id', $users[$i]['id'])
                ->first();

            $role = Role::query()
                ->where('id', 1)
                ->first();

            if ($user == null) {
                $user = User::create([
                	'id' => $users[$i]['id'],
                	'name' => $users[$i]['name'],
                	'email' => $users[$i]['email'],
                	'password' => $users[$i]['password']
                ]);

                $user->roles()->attach($role);
            }
        }
    }
}
