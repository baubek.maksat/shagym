<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ComplainView;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            //	LangSeeder::class,
            //	RoleSeeder::class,
            //	UserSeeder::class,
            RegionSeeder::class,
            SettlementSeeder::class,
            //	ComplainViewSeeder::class,
            //	ComplainStatusSeeder::class,
            //	NegativeSeeder::class 
    	]);
    }
}
