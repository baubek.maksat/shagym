<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ComplaintStatus;

class ComplainStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $complain_statuses = [
            [
                'id' => 1,
                'name_ru' => 'Не обработан'
            ],[
                'id' => 2,
                'name_ru' => 'Обработан'
            ]
        ];

        if (count($complain_statuses) > 0) {
            for ($i = 0; $i < count($complain_statuses); $i++) { 
                $complain_status = ComplaintStatus::query()
                    ->where('id', $complain_statuses[$i]['id'])
                    ->first();

                if ($complain_status == null) {
                    $complain_status = ComplaintStatus::create([
                    	'id' => $complain_statuses[$i]['id']
                    ]);

	                $complain_status->translation()->create([
	                	'lang' => 'ru',
	                	'name' => $complain_statuses[$i]['name_ru']
	                ]);
                }
            }
        }
    }
}
