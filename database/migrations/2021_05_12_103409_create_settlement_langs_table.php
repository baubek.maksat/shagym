<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettlementLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settlement_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('lang');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('settlements');
            $table->foreign('lang')->references('slug')->on('langs');

            $table->primary([
                'id',
                'lang'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlement_langs');
    }
}
