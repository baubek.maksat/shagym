<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('view_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('settlement_id')->nullable();
            $table->string('name');
            $table->string('subname')->nullable();
            $table->date('date_start');
            $table->date('date_end');
            $table->text('comment')->nullable();
            $table->string('score')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('view_id')->references('id')->on('complaint_views');
            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('status_id')->references('id')->on('complaint_statuses');
            $table->foreign('settlement_id')->references('id')->on('settlements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
