<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintViewsNegativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaint_views_negatives', function (Blueprint $table) {
            $table->unsignedBigInteger('complaint_view_id');
            $table->unsignedBigInteger('negative_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('complaint_view_id')->references('id')->on('complaint_views');
            $table->foreign('negative_id')->references('id')->on('negatives');

            $table->primary([
                'complaint_view_id',
                'negative_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaint_views_negatives');
    }
}
