<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsNegativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints_negatives', function (Blueprint $table) {
            $table->unsignedBigInteger('complaint_id');
            $table->unsignedBigInteger('negative_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('complaint_id')->references('id')->on('complaints');
            $table->foreign('negative_id')->references('id')->on('negatives');

            $table->primary([
                'complaint_id',
                'negative_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints_negatives');
    }
}
