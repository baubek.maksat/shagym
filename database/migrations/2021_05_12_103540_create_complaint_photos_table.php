<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaint_photos', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('src');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('complaints');

            $table->primary([
                'id',
                'src'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaint_photos');
    }
}
