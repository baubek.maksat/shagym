<?php

return [
    'Hotel_inn' => 'Гостиница/санаторий/другое место размещения',
    'travel agency' => 'Турфирма',
    
    'What do' => 'На что хотите пожаловаться?',
    'Guide' => 'Гид',
    'Transport company' => 'Транспортная компания',
    'Restaurant, café, etc' => 'Ресторан/кафе/другой пункт питания',
    'Entertainment services' => 'Услуги развлечения',
    'Other' => 'Другое',
    'Restroom' => 'Придорожный сервис/туалет',

    's_1_What is the name of the hotel you are complaining about?' => 'Как называется гостиница/санаторий/место размещения, на который вы жалуетесь?*',
    's_1_required field' => 'Поле обязательно для заполнения!',
    's_1_Where is the hotel located?' => 'Где находится гостиница/санаторий/место размещения?',
    's_1_Dates of your stay' => 'Даты вашего проживания',
    's_1_What are you complaining about?' => 'Что вам не понравилось?',
    's_1_Please give details of complaint' => 'Напишите, пожалуйста, подробнее о своей жалобе',
    's_1_Upload photos' => 'Прикрепить фотографии',
    's_1_btn_foto' => 'Выберите файл',


    's_2_What is the name of the travel agency you are complaining about?' => 'Как называется турфирма, на которую вы жалуетесь?',
    's_2_What tour did you purchase from this travel agency? (tour name)' => 'Какой тур вы приобрели у этой турфирмы?(название тура)',
    's_2_Where is the travel agency located?' => 'Где находится турфирма?',
    's_2_Tour dates' => 'Даты проведения тура',
    's_2_What are you complaining about?' => ' Что вам не понравилось?',
    's_2_Please give details of complaint' => ' Напишите, пожалуйста, подробнее о своей жалобе',



    's_3_What is the guide’s full name?' => 'Как зовут гида?',
    's_3_Where did the tour take place?' => 'В каком регионе проходил тур?',


    's_4_What is the name of the transport company you are complaining about?' => 'Как называется транспортная компания, на которую вы жалуетесь?',
    's_4_Where is the company located?' => 'В каком регионе находится компания?',
    's_4_When did the incident you are complaining about happened?' => 'Даты проведения тура',
   
    's_5_What is the name of the restaurant/café you’re complaining about?' => 'Как называется транспортная компания, на которую вы жалуетесь?Название ресторана/кафе/другого заведения?',
    's_5_Where is the restaurant located?' => 'Где находится заведение?',
    's_5_Date of visit' => 'Дата посещения',

    's_6_What is the name of the entertainment facility you are complaining about?' => 'Название заведения',
    's_6_Where is the entertainment facility located?' => 'Где находится заведение?',


    's_7_What is the name of the organization you are complaining about?' => 'Название организации, на которую вы жалуетесь',
    's_7_Where is the organization located?' => 'Где находится заведение?',
    

    's_8_name' => 'Название придорожного сервиса или места, где находится туалет',
    's_8_name2' => '(просим дать наиболее точное название, чтобы можно было идентифицировать объект)',
    's_8_location' => 'Где находится придорожный сервис или туалет?',
   
    's_9_name0' => ' Заполните свои данные для отправки жалобы',
    's_9_name' => 'Ваше имя',
    's_9_Phone number' => 'Телефон',
    's_9_sub' => 'Отправить жалобу',

    'next' => 'Далее',
    'prev' => 'Назад',
    
    'reg' => 'Регион',
    'reg_2' => 'Населенный пункт или курорт',

    'text_bot_1' => '   На нашем сайте вы можете оставить отзыв или жалобу на все туристские услуги, если вам не понравился отдых в Казахстане. Ваши отзывы помогут нам улучшать сервис внутреннего туризма. Отзывы будут использованы как для выявления и точечного устранения недостатков, так и для анализа системных проблем в обобщенном виде.' ,
    'text_bot_2' => ' Выше вы можете выбрать вид услуги, на который хотели бы пожаловаться. Далее нужно заполнить простую форму и оставить свои контакты, для того, чтобы мы могли связаться с вами при необходимости.' ,
    'text_bot_3' => ' Благодарим за содействие!',

    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',


    'success_title' => 'Спасибо, Ваша жалоба принята!',
    'success_linck' => 'Подать еще одну жалобу',

    

    'menu1' => 'КУДА ПОЕХАТЬ',
    'menu2' => 'ЧЕМ ЗАНЯТЬСЯ',
    'menu3' => 'ЗАПЛАНИРУЙТЕ ПОЕЗДКУ',
    'menu4' => 'Для бизнеса',
    'menu5' => 'eQonaq',

    'menu6' => 'Золотая Орда',
    'menu7' => 'Фотобанк',
    'menu8' => 'SafeTravels',


    'menu10' => 'Список полезных веб-ресурсов',
    'menu11' => 'Партнеры',
    'menu12' => 'Соглашение',
    'menu13' => 'О проекте',
    'menu14' => 'Контакты',
    'menu15' => 'Обратная связь',

    

    'menu17' => 'Министерство культуры и спорта Республики Казахстан',
    'menu18' => 'Акционерное общество «Национальная компания «Kazakh Tourism»',



];
