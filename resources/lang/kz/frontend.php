<?php

return [
    'Hotel_inn' => 'Қонақ үй / демалыс орталығы / шипажай / басқа орындар ',
    'travel agency' => 'Туристік агенттік (туроператор, турагент)',
    'What do' => 'Сіз неге шағымданғыңыз келеді?',
    'Guide' => 'Нұсқаулық',
    'Transport company' => 'Көлік компаниясы',
    'Restaurant, café, etc' => 'Мейрамхана / кафе / басқа тамақтану орыны',
    'Entertainment services' => 'Ойын-сауық қызметтері',
    'Other' => 'Басқа',
    'Restroom' => 'Жол бойындағы қызмет көрсету/ дәретхана',
  
    'text_bot_1' => 'Егер сізге Қазақстандағы демалыс ұнамаса, біздің сайтта сіз барлық туристік қызметтерге өз пікіріңізді немесе шағымыңызды қалдыра аласыз. Сіздің пікіріңіз ішкі туризм қызметін жақсартуға көмектеседі. Пікірлер кемшіліктерді анықтау және жою үшін де, жүйелік проблемаларды жалпыланған түрде талдау үшін де қолданылады.' ,
    'text_bot_2' => 'Жоғарыда сіз шағымданғыңыз келетін қызмет түрін таңдай аласыз. Әрі қарай, қажет болған жағдайда сізбен байланысу үшін қарапайым нысанды толтырып, контактілерді қалдыру керек.' ,
    'text_bot_3' => 'Көмегіңіз үшін рақмет!',
    'reg' => 'Аймақ',
    'reg_2' => ' Елді мекен немесе шипажай',
   
    's_1_What is the name of the hotel you are complaining about?' => 'Сіз шағымданып жатқан қонақ үйдің аты қалай аталады?',
    's_1_required field' => 'Міндетті түрде толтыру жиегі!',
    's_1_Where is the hotel located?' => 'Қонақ үй қай жерде орналасқан? ',
    's_1_Dates of your stay' => 'Сіздің қонақ үйде тұратын күндеріңіз',
    's_1_What are you complaining about?' => 'Сізге не ұнамады?',
    's_1_Please give details of complaint' => 'Шағымыңыз туралы толығырақ жазыңыз',
    's_1_Upload photos' => 'Фотосуреттерді тіркеңіз',
    's_1_btn_foto' => 'Файлды таңдаңыз',


    's_2_What is the name of the travel agency you are complaining about?' => 'Сіз шағымданатын туристік агенттік қалай аталады?',
    's_2_What tour did you purchase from this travel agency? (tour name)' => 'Сіз осы туристік агенттіктен қандай тур сатып алдыңыз? (тур атауы) ',
    's_2_Where is the travel agency located?' => 'Туристік агенттік қай жерде орналасқан? ',
    's_2_Tour dates' => 'Турдың өтетін күндері',
    's_2_What are you complaining about?' => 'Сізге не ұнамады?',
    's_2_Please give details of complaint' => 'Шағымыңыз туралы толығырақ жазыңыз ',


    's_3_What is the guide’s full name?' => 'Нұсқаулық қалай аталады? ',
    's_3_Where did the tour take place?' => 'Экскурсия қай аймақта өтті? ',

    's_4_What is the name of the transport company you are complaining about?' => 'Сіз шағымданып отырған көлік тасымалдау компаниясы қалай аталады?',
    's_4_Where is the company located?' => 'Компания қай аймақта орналасқан? ',
    's_4_When did the incident you are complaining about happened?' => 'Турдың өтетін күндері',


    's_5_What is the name of the restaurant/café you’re complaining about?' => 'Мейрамхананың / кафенің / басқа мекеменің атауы?',
    's_5_Where is the restaurant located?' => 'Орналасқан орны?',
    's_5_Date of visit' => 'Бару күндері',

    's_6_What is the name of the entertainment facility you are complaining about?' => 'Мекеменің атауы?',
    's_6_Where is the entertainment facility located?' => 'Мекеме орналасқан орын?',

    's_7_What is the name of the organization you are complaining about?' => 'Сіз шағымданатын ұйымның атауы ',
    's_7_Where is the organization located?' => 'Ұйымның орналасқан орны? ',


    's_8_name' => 'Жол бойындағы қызметтердің атаулары немесе дәретханалар орналасқан жерлер',
    's_8_name2' => '(нысанды анықтай алу үшін атауын нақты беріңіз)',
    's_8_location' => 'Жол бойындағы қызмет көрсету немесе дәретхана қай жерде орналасқан?',

    's_9_name0' => 'Шағым жіберу үшін жеке мәліметтеріңізді толтырыңыз',
    's_9_name' => 'Сіздің атыңыз',
    's_9_Phone number' => 'Телефон',
    's_9_sub' => 'Жіберу',

    'next' => 'Келесі',
    'prev' => 'Артқа',

    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    
    'success_title' => 'Рахмет, сіздің шағымыңыз қабылданды!',
    'success_linck' => 'Басқа шағым',


    'menu1' => 'БАҒЫТТАР',
    'menu2' => 'САЯХАТ',
    'menu3' => 'ЖОСПАРЛАУ',
    'menu4' => 'Бизнес үшін',
    'menu5' => 'eQonaq',

    'menu6' => 'Алтын Орда',
    'menu7' => 'Фотобанк',
    'menu8' => 'SafeTravels',


    'menu10' => 'Пайдалы веб-ресурстар тізімі',
    'menu11' => 'Әріптестер',
    'menu12' => 'Келісім',
    'menu13' => 'Жоба туралы',
    'menu14' => 'Байланыстар',
    'menu15' => 'Кері байланыс',


    
    'menu17' => 'Қазақстан Республикасының мәдениет және спорт министрлігі',
    'menu18' => '«Kazakh Tourism» ұлттық компаниясы» акционерлік қоғамы',




];
