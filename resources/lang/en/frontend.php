<?php

return [
    'Hotel_inn' => 'Hotel, inn, recreation center, health resort, or other place of accommodation',
    'travel agency' => 'Travel agency (tour operator, travel agent)',
    'What do' => 'What do you want to complain about?',
    'Guide' => 'Guide',
    'Transport company' => 'Transport company',
    'Restaurant, café, etc' => 'Restaurant, café, etc',
    'Entertainment services' => 'Entertainment services',
    'Other' => 'Other',
    'Restroom' => 'Roadside service / toilet',

    'reg' => 'Region',
    'reg_2' => 'Locality or resort',
    'text_bot_1' => 'On our website you can leave a review or complaint about all tourist services if you were not satisfied with your vacation in Kazakhstan. Your feedback will help us to improve our domestic tourism service. We can identify and pinpoint shortcomings with your feedback. It also needs to analyze systemic problems in a generalized form.' ,
    'text_bot_2' => 'On the top you can select the type of the service you would like to complain. Then you need to fill out a simple form and leave your contacts to contact you if necessary.' ,
    'text_bot_3' => 'Thank you for your assistance!',


    's_1_What is the name of the hotel you are complaining about?' => 'What is the name of the hotel you are complaining about?',
    's_1_required field' => 'This field is required!',
    's_1_Where is the hotel located?' => 'Where is the hotel located?',
    's_1_Dates of your stay' => 'Dates of your stay',
    's_1_What are you complaining about?' => 'What are you complaining about?',
    's_1_Please give details of complaint' => 'Please give details of complaint',
    's_1_Upload photos' => 'Upload photos',
    's_1_btn_foto' => 'Select a file',



    's_2_What is the name of the travel agency you are complaining about?' => 'What is the name of the travel agency you are complaining about?',
    's_2_What tour did you purchase from this travel agency? (tour name)' => 'What tour did you purchase from this travel agency? (tour name)',
    's_2_Where is the travel agency located?' => 'Where is the travel agency located?',
    's_2_Tour dates' => 'Tour dates',
    's_2_What are you complaining about?' => 'What are you complaining about?',
    's_2_Please give details of complaint' => 'Please give details of complaint',


    's_3_What is the guide’s full name?' => 'What is the guide’s full name?',
    's_3_Where did the tour take place?' => 'Where did the tour take place?',


    's_4_What is the name of the transport company you are complaining about?' => 'What is the name of the transport company you are complaining about?',
    's_4_Where is the company located?' => 'Where is the company located?',
    's_4_When did the incident you are complaining about happened?' => 'Tour dates',


    's_5_What is the name of the restaurant/café you’re complaining about?' => 'What is the name of the restaurant/café you’re complaining about?',
    's_5_Where is the restaurant located?' => 'Where is the restaurant located?',
    's_5_Date of visit' => 'Date of visit',


    's_6_What is the name of the entertainment facility you are complaining about?' => 'What is the name of the entertainment facility you are complaining about?',
    's_6_Where is the entertainment facility located?' => 'Where is the entertainment facility located?',

    's_7_What is the name of the organization you are complaining about?' => 'What is the name of the organization you are complaining about?',
    's_7_Where is the organization located?' => 'Where is the organization located?',

    's_8_name' => 'Name of the roadside service or location where the toilet is located',
    's_8_name2' => '(please give the most accurate name so that you can identify the object)',
    's_8_location' => 'Where is the roadside service or toilet located?',
   
    's_9_name0' => 'Fill in your details to send a complaint',
    's_9_name' => 'Your name',
    's_9_Phone number' => 'Phone number',
    's_9_sub' => 'OK',


    'next' => 'Next',
    'prev' => 'Previous',





    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',


    'success_title' => 'Thank you, your complaint has been submitted!',
    'success_linck' => 'Submit another complaint',
    

    'menu1' => 'WHERE TO GO',
    'menu2' => 'WHAT TO DO',
    'menu3' => 'PLAN YOUR TRIP',
    'menu4' => 'For Business',
    'menu5' => 'eQonaq',

    'menu6' => 'Altyn Orda',
    'menu7' => 'Photobank',
    'menu8' => 'SafeTravels',


    'menu10' => 'Useful web resources',
    'menu11' => 'Partners',
    'menu12' => 'Agreement',
    'menu13' => 'About project',
    'menu14' => 'Contacts',
    'menu15' => 'Feedback',

    'menu17' => 'Ministry of culture and sport of the Republic of Kazakhstan',
    'menu18' => 'National Company Kazakh Tourism Joint-Stock Company ',

];
