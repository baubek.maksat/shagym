@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Жалоба</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/complaint">Жалобы</a></li>
                    </ol>
                </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <form action="{{ route('backend.complaints.update', ['complaint' => $complaint->id]) }}" id="edit_form" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="view_id" value="{{ $complaint->view_id }}">

                    <div id="form_message"></div>

                    <div class="d-flex">
                        <div class="nav-pills ml-auto p-2">
                            <button type="submit" class="btn btn-info">Сохранить</button>
                        </div>
                    </div>
                    
                    

                    <div class="row">


                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Называние {{ $complaint->view->translation->name }}</label>
                                <input type="text" class="form-control" name="name" value="{{ $complaint->name }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label>Теги</label>
                                        <input type="text" class="form-control" name="tags" data-role="tagsinput" value="teg1, teg2">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Баллы</label>
                                        <input type="number" class="form-control" name="score" value="{{ $complaint->score }}">
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-sm-12">
                            <div class="row">

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Регион</label>
                                        <select class="custom-select rounded-0" id="region" name="region_id">
                                            @foreach($regions as $region)
                                                @if($region->id == $complaint->region->id)
                                                    <option value="{{ $region->id }}" data-region="{{ $region->id }}" selected="">{{ $region->translation->name }}</option>
                                                @else
                                                    <option value="{{ $region->id }}" data-region="{{ $region->id }}">{{ $region->translation->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Населенный пункт или курорт</label>
                                        <select class="custom-select rounded-0" id="city" name="settlement_id">
                                            @foreach($settlements as $settlement)
                                                @if($settlement->id == $complaint->settlement->id)
                                                    <option value="{{ $settlement->id }}" data-city="{{ $settlement->region_id }}" selected="">{{ $settlement->translation->name }}</option>
                                                @else
                                                    <option value="{{ $settlement->id }}" data-city="{{ $settlement->region_id }}">{{ $settlement->translation->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Даты проживания</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="reservation" value="{{ $complaint->date_start }} - {{ $complaint->date_end }}">
                                            <input type="hidden" name="date_start" id="date_start" value="{{ $complaint->date_start }}">
                                            <input type="hidden" name="date_end" id="date_end" value="{{ $complaint->date_end }}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-4">

                            @foreach($complaint->negatives as $negative)
                                <a href="" class="badge badge-success"></a>
                                <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                        <input type="checkbox" id="checkboxPrimary-{{ $negative->id }}" checked="" name="negatives[]" value="{{ $negative->id }}">
                                        <label for="checkboxPrimary-{{ $negative->id }}">
                                            {{ $negative->translation->name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Подробнее о жалобе</label>
                                <textarea class="form-control" rows="6" name="comment">{{ $complaint->comment }}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-7">

                            <div class="form-group">
                                <label>Файлы фото</label>
                                <div class="row mb-3">

                                    @foreach($complaint->photos as $photo)
                                        <div class="col-sm-3 photo__remove--label">
                                            <label style="position: relative;">
                                                <a data-fancybox="gallery" href="{{ asset($photo->src) }}">
                                                    <img class="img-fluid" src="{{ asset($photo->src) }}" alt="Photo">
                                                </a>
                                                <input type="hidden" name="photos[]" value="{{ asset($photo->src) }}">
                                                <a class="btn btn-danger btn-sm photo__remove"  style="position: absolute;right: 10px;top: 10px;">
                                                    x
                                                </a>
                                            </label>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5">

                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" class="form-control" disabled="" value="{{ $сlient->name }}">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" disabled="" value="{{ $сlient->email }}">
                            </div>
                            <div class="form-group">
                                <label>Телефон</label>
                                <input type="text" class="form-control" disabled="" value="{{ $сlient->phone }}">
                            </div>

                        </div>


                        
                    </div>

                </form>


                
                
                

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


@endsection

@section('script')
    <style>
        .bootstrap-tagsinput span.badge.badge-info {
            margin-right: 5px;
        }

        button.ui-datepicker-trigger {
            position: absolute;
            right: 10px;
            padding: 0;
            background: transparent;
            border: 0;
            top: 50%;
            transform: translateY(-50%);
        }
        .form-control {
            border-radius: .25rem !important;
        }

    </style>
    <script>

        $('.photo__remove').click(function(){
            $(this).parents('.photo__remove--label').remove();
        });
    
        $('#reservation').datepicker({
            range: 'period', // режим - выбор периода
            numberOfMonths: 2,
            dateFormat: "yy-mm-dd",

            showOn: "button",
            buttonImage: "/img/calendar.svg",

            onSelect: function (dateText, inst, extensionRange) {
                // extensionRange - объект расширения
                $('#reservation').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
                $('#date_start').val(extensionRange.startDateText);
                $('#date_end').val(extensionRange.endDateText);

            }
        });
        // $('#reservation').daterangepicker({
        //     minDate: moment().subtract(2, 'years'),
        //     callback: function (startDate, endDate, period) {
        //         $('#reservation').val(startDate.format('L') + ' – ' + endDate.format('L'));
        //         $('#date_start').val(startDate.format('L'));
        //         $('#date_end').val(endDate.format('L'));
        //     }
        // });


        jQuery.fn.chained = function (parent_selector, options) {
            return this.each(function () {
                var self = this;                                        /* Save this to self because this changes when scope changes. */
                var backup = jQuery(self).clone();
                jQuery(parent_selector).each(function () {               /* Handles maximum two parents now. */
                    jQuery(this).bind("change", function () {
                        jQuery(self).html(backup.html());
                        var selected = "";                                      /* If multiple parents build classname like foo\bar. */
                        jQuery(parent_selector).each(function () { selected += "\\" + jQuery(":selected", this).val(); });
                        selected = selected.substr(1);                          /* Also check for first parent without subclassing. */ /* TODO: This should be dynamic and check for each parent */ /* without subclassing. */
                        var first = jQuery(parent_selector).first();
                        var selected_first = jQuery(":selected", first).data("region");
                        jQuery("option", self).each(function () {
                            if (jQuery(this).data("city") != selected_first) { jQuery(this).remove(); }
                        });
                        // if (1 == jQuery("option", self).size() && jQuery(self).val() === "") { jQuery(self).attr("disabled", "disabled"); }  /* If we have only the default value disable select. */
                        // else { jQuery(self).removeAttr("disabled"); }
                        jQuery(self).trigger("change");
                    });
                    if (!jQuery("option:selected", this).length) { jQuery("option", this).first().attr("selected", "selected"); }       /* Force IE to see something selected on first page load, */ /* unless something is already selected */
                    jQuery(this).trigger("change");                        /* Force updating the children. */
                });
            });
        };
        jQuery.fn.chainedTo = jQuery.fn.chained;               /* Alias for those who like to use more English like syntax. */
        jQuery(document).ready(function () { jQuery("#city").chained("#region"); });


        jQuery("#edit_form").submit(function () {
            var edit_form = jQuery('#edit_form');
            
            jQuery.ajax({
                type: "POST",
                url: edit_form.attr("action"),
                data: edit_form.serialize(),
                success: function (data) {
                    console.log(data);
                    if(data.code == 200){
                        edit_form.find(".input-linck").removeClass('required__error');
                        edit_form.find(".input-linck").siblings('.input-required').hide();
                        // $(location).attr('href','/administrator/complaints');
                        $("#form_message").html('<div class="alert alert-success">Сохранино</div>');
                    } else {
                        edit_form.find(".input-linck").addClass('required__error');
                        edit_form.find(".input-linck").siblings('.input-required').show();
                        $("#form_message").html('<div class="alert alert-danger">Не сохранино</div>');
                    }
                },
                error: function (jqXHR, text, error) {
                    // jQuery(formNm2).html(error);
                    console.log(error);
                }
            });
            return false;
        });
        
    </script>
@endsection