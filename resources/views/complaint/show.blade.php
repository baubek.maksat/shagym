@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Жалоба</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('frontend.administrator.complaints.index') }}">Жалобы</a></li>
                    </ol>
                </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">


                <div class="row">


                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Называние {{ $complaint->view->translation->name }}</label>
                            <input type="text" class="form-control" name="name" disabled="" value="{{ $complaint->name }}">
                        </div>
                    </div>
                    @isset($complaint->subname)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Называние турфирмы{{ $complaint->view->translation->name }}</label>
                                <input type="text" class="form-control" name="name" disabled="" value="{{ $complaint->subname }}">
                            </div>
                        </div>
                    @endisset
                    
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Теги</label>
                                    <input type="text" class="form-control" name="tags" data-role="tagsinput" value="teg1, teg2" disabled="">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Баллы</label>
                                    <input type="number" class="form-control" name="name" value="{{ $complaint->score }}" disabled="">
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="col-sm-12">
                        <div class="row">

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Регион</label>
                                    <select class="custom-select rounded-0" name="region_id" disabled="">
                                        <option>{{ $complaint->region->translation->name }}</option>
                                        <option>Value 2</option>
                                        <option>Value 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Населенный пункт или курорт</label>
                                    <select class="custom-select rounded-0" name="settlement_id" disabled="">
                                        <option>{{ $complaint->settlement->translation->name }}</option>
                                        <option>Value 2</option>
                                        <option>Value 3</option>
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Даты проживания</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" id="reservation" disabled="">
                                        <input type="hidden" name="date_start" id="date_start" disabled="" value="{{ $complaint->date_start }}">
                                        <input type="hidden" name="date_end" id="date_end" value="{{ $complaint->date_end }}" disabled="">
                                    </div>
                                </div>
                            </div> --}}

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Даты проживания</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="reservation" value="{{ $complaint->date_start }} - {{ $complaint->date_end }}" disabled>
                                        <input type="hidden" name="date_start" id="date_start" value="{{ $complaint->date_start }}" disabled>
                                        <input type="hidden" name="date_end" id="date_end" value="{{ $complaint->date_end }}" disabled>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="col-sm-4">

                        <label>Что не понравилось</label>

                        @foreach($complaint->negatives as $negative)
                            <a href="" class="badge badge-success"></a>
                            <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="checkboxPrimary1" checked="" name="negatives[]" disabled="">
                                <label for="checkboxPrimary1">
                                    {{ $negative->translation->name }}
                                </label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label>Подробнее о жалобе</label>
                            <textarea class="form-control" rows="6" disabled="">{{ $complaint->comment }}</textarea>
                        </div>
                    </div>

                    <div class="col-sm-7">

                        <div class="form-group">
                            <label>Файлы фото</label>
                            <div class="row mb-3">
                                @foreach($complaint->photos as $photo)
                                    <div class="col-sm-3">
                                        <a data-fancybox="gallery" href="{{ asset($photo->src) }}">
                                            <img class="img-fluid" src="{{ asset($photo->src) }}" alt="Photo">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5">

                        <div class="form-group">
                            <label>Имя</label>
                            <input type="text" class="form-control" disabled="" value="{{ $сlient->name }}">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" disabled="" value="{{ $сlient->email }}">
                        </div>
                        <div class="form-group">
                            <label>Телефон</label>
                            <input type="text" class="form-control" disabled="" value="{{ $сlient->phone }}">
                        </div>

                    </div>

                    
                </div>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


@endsection

@section('script')
    <style>
        .bootstrap-tagsinput span.badge.badge-info {
            margin-right: 5px;
        }
        .bootstrap-tagsinput input[type="text"] {
            display: none;
        }
        .bootstrap-tagsinput {
            background-color: #e9ecef;
        }
        .bootstrap-tagsinput span[data-role="remove"] {
            display: none;
        }
    </style>
    <script>
    
        
        
        $('#reservation').datepicker({
            range: 'period', // режим - выбор периода
            numberOfMonths: 2,
            dateFormat: "yy-mm-dd",

            // showOn: "button",
            buttonImage: "",

            onSelect: function (dateText, inst, extensionRange) {
                // extensionRange - объект расширения
                $('#reservation').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
                $('#date_start').val(extensionRange.startDateText);
                $('#date_end').val(extensionRange.endDateText);

            }
        });
        // $('#reservation').daterangepicker({
        //     callback: function (startDate, endDate, period) {
        //         $('#reservation').val(startDate.format('L') + ' – ' + endDate.format('L'));
        //         $('#date_start').val(startDate.format('L'));
        //         $('#date_end').val(endDate.format('L'));
                
        //     }
        // })
        
    </script>
@endsection