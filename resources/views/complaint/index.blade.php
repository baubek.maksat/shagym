@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Жалобы</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/complaint">Жалобы</a></li>
                    </ol>
                </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">

                        @if(session('success'))
                            <div class="row justify-content-center">
                                <div class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"></span>
                                        </button>
                                        {{session()->get('success')}}
                                    </div>
                                </div>
                            </div>
                        @endif

                        
                        <div class="d-flex">
                            <div class="nav-pills mr-auto p-2">
                                <p>
                                    Средний бал: <strong>{{ $complaints_score['middle'] }}</strong>; 
                                    Общий бал: <strong>{{ $complaints_score['sum'] }}</strong>;
                                </p>
                            </div>

                            <div class="nav-pills ml-auto p-2">
                                <a href="#" class="btn btn-primary">
                                    <i class="fas fa-table"></i>
                                    Импорт
                                </a>
                            </div>
                        </div>
                        <br>


                        <div class="card card-default">

                            <div class="card-header">
                                <h3 class="card-title">Фильтр</h3>
                                <div class="card-tools">
                                    {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button> --}}
                                </div>
                            </div>

                            <form>
                                <div class="card-body" style="display: block;">

                                    <div class="row">

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>На что жалоба</label>
                                                <select class="custom-select rounded-0" name="view_id">
                                                    <option value="0">Не выбран</option>
                                                    @isset($complaint_views)
                                                        @foreach($complaint_views as $complaint_view)
                                                            @if($request->view_id == $complaint_view->id)
                                                                <option value="{{ $complaint_view->id }}" selected="">{{ $complaint_view->translation->name }}</option>
                                                            @else
                                                                <option value="{{ $complaint_view->id }}">{{ $complaint_view->translation->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Что не понравилось</label>
                                                <select class="custom-select rounded-0"   name="negative_id">
                                                    <option value="0">Не выбран</option>
                                                    @isset($negatives)
                                                        @foreach($negatives as $negative_view)
                                                            @if($request->negative_id == $negative_view->id)
                                                                <option value="{{ $negative_view->id }}" selected="">{{ $negative_view->translation->name }}</option>
                                                            @else
                                                                <option value="{{ $negative_view->id }}">{{ $negative_view->translation->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Регион</label>
                                                <select class="custom-select rounded-0" name="region_id">
                                                    <option value="0">Не выбран</option>
                                                    @isset($regions)
                                                        @foreach($regions as $region)
                                                            @if($request->region_id == $region->id)
                                                                <option value="{{ $region->id }}" selected="">{{ isset($region->translation) ? $region->translation->name : '' }}</option>
                                                            @else
                                                                <option value="{{ $region->id }}">{{ isset($region->translation) ? $region->translation->name : '' }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Населенный пункт</label>
                                                <select class="custom-select rounded-0" name="settlement_id">
                                                    <option value="0">Не выбран</option>
                                                    @isset($settlements)
                                                        @foreach($settlements as $settlement)
                                                            @if($request->settlement_id == $settlement->id)
                                                                <option value="{{ $settlement->id }}" selected="">{{ $settlement->translation->name }}</option>
                                                            @else
                                                                <option value="{{ $settlement->id }}">{{ $settlement->translation->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Теги</label>
                                                <select class="custom-select rounded-0">
                                                    <option value="0">Не выбран</option>
                                                    <option value="0">teg1</option>
                                                    <option value="0">teg2</option>
                                                    <option value="0">teg3</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label>Дата подачи жалобы</label>

                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <div class="input-group-prepend" data-target="#reservationdate" data-toggle="datetimepicker">
                                                        <span class="input-group-text">
                                                          <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate" name="created_at"  value="{{ substr($request->created_at, 0, 10) }}" />
                                                    {{-- {{ substr($request->created_at, 6, 4) }}-{{ substr($request->created_at, 3, 2) }}-{{ substr($request->created_at, 0, 2) }} --}}
                                                    
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <div class="form-group">
                                                <label>Баллы</label>
                                                <input type="number" class="form-control" name="score" value="{{ $request->score }}">
                                            </div>
                                        </div>
                                        
                                        

                                    </div>

                                </div>
                                <!-- /.card-body -->
                
                                <div class="card-footer">
                                    <div class="d-flex">
                                        <div class="nav-pills mr-auto p-2">
                                            <button type="submit" class="btn btn-primary">Фильтровать</button>
                                            {{-- <button type="submit" class="btn btn-warning">Сбросить</button> --}}
                                            <a href="{{ route('frontend.administrator.complaints.index') }}"  class="btn btn-warning">Сбросить</a>
                                        </div>
                                        <div class="nav-pills ml-auto p-2">
                                            <a href="{{ route('frontend.administrator.complaints.export.csv', $request->query()) }}" class="btn btn-info">
                                                <i class="fas fa-table"></i>
                                                Экспорт в csv
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->

                       


                        <div class="card">
                    
                            <table class="table table-hover" id="table_id">
                                <thead>
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        <th>На что жалоба</th>
                                        <th>Название</th>
                                        <th>Имя туриста</th>
                                        <th>Регион</th>
                                        <th>Населенный пункт</th>
                                        <th>Дата подачи жалобы</th>
                                        <th style="width: 350px;">Что не понравилось</th>
                                        <th>Баллы</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($complaints as $complaint)
                                        <tr>
                                            {{-- <td>{{ $complaint->id }}</td> --}}
                                            <td>
                                                {{ $complaint->view->translation->name }}
                                                <p>
                                                    <a href="" class="badge badge-success">teg1</a>
                                                    <a href="" class="badge badge-success">teg2</a>
                                                </p>
                                            </td>
                                            <td>
                                                {{ $complaint->name }}
                                            </td>
                                            <td>
                                                <a href="{{ route('frontend.administrator.complaints.index', ['client_id' => $complaint->client->id]) }}">{{ $complaint->client->name }}</a>
                                            </td>
                                            <td>{{ $complaint->region->translation->name }}</td>
                                            <td>{{ $complaint->settlement->translation->name }}</td>
                                            <td>{{ substr($complaint->created_at, 0, 10) }}</td>
                                            <td>
                                                @foreach($complaint->negatives as $negative)
                                                    <a href="" class="badge badge-success">{{ $negative->translation->name }}</a>
                                                @endforeach
                                            </td>
                                            <td>{{ $complaint->score ?: 0 }}</td>
                                            <td>
                                                <a href="{{ route('frontend.administrator.complaints.show', ['complaint' => $complaint->id]) }}" class="btn btn-outline-info">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                                <a href="{{ route('frontend.administrator.complaints.edit', ['complaint' => $complaint->id]) }}" class="btn btn-outline-primary">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <a href="{{ route('backend.complaints.destroy', ['complaint' => $complaint->id]) }}" class="btn btn-outline-danger">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>


@endsection


@section('script')
    <script>
        $(function () {
 
            $('#table_id').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });

            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
                // format: 'L'
            });
            
        });

      </script>

      <style>
            table#table_id {
                margin: 0 !important;
            }
            div#table_id_info {
                padding-left: 20px;
                padding-bottom: 20px;
                margin-top: 20px;
            }
            div#table_id_paginate {
                padding-right: 20px;
                margin-bottom: 20px;
                margin-top: 20px;
            }
      </style>
@endsection
