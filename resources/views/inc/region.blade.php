<div class="row">
    <div class="col-lg-6">
        <div class="form__listitem--input">
            <select class="form-control input-select" id="region" required="" name="region_id" aria-label="Регион ">
                <option selected value="" data-region="all"> {{ __('frontend.reg') }}</option>
                @foreach ($regions as $item)
                    <option value="{{ $item->id }}" data-region="{{ $item->id }}">{{ isset($item->translation) ? $item->translation->name : '' }}</option>
                @endforeach
            </select>
            <span class="input-required"> {{ __('frontend.s_1_required field') }}</span>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form__listitem--input">
            <select class="form-control input-select" id="city" required="" name="settlement_id" aria-label="{{ __('frontend.reg_2') }}">
                <option selected value="" data-city="all">{{ __('frontend.reg_2') }}</option>
                @foreach ($settlements as $set_item)
                    <option value="{{ $set_item->id }}" data-city="{{ $set_item->region_id }}">{{ $set_item->translation->name }}</option>
                @endforeach
            </select>
            <span class="input-required"> {{ __('frontend.s_1_required field') }}</span>
        </div>
    </div>
</div>


