@foreach ($complaint_view->negatives as $negative)
    <label class="checkbox__input checkbox__input--{{ $negative->translation->id }}">
        <input type="checkbox" class="input-checkbox" name="negatives[]" value="{{ $negative->translation->id }}">
        <span></span>
        {{ $negative->translation->name }}
    </label>
@endforeach

<div class="liked__other" style="display: none;">
    <textarea class=" input-linck" name="comment" rows="4"></textarea>
    <span class="input-required"> {{ __('frontend.s_1_required field') }}</span>
</div>