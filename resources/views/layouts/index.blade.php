<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>На что хотите пожаловаться?</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/swiper.min.css">
    <link rel="stylesheet" href="/css/component.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="/css/style.css?v=0.0.3">
    <link rel="stylesheet" href="/css/media.css?v=0.0.3">

</head>

<body class="home-page">


    <header>

        <div class="container">
            <div class="header__row">

                <div class="header__left">

                    <div class="header__logo">
                        <a href="https://kazakhstan.travel/">
                            <img src="/img/logo.svg" alt="">
                        </a>
                    </div>
                    <div class="header__menu">
                        <nav class="nav">
                            <a class="nav-link" href="https://kazakhstan.travel/">{{ __('frontend.menu1') }}</a>
                            <a class="nav-link" href="https://kazakhstan.travel/">{{ __('frontend.menu2') }}</a>
                            <a class="nav-link" href="https://kazakhstan.travel/">{{ __('frontend.menu3') }}</a>
                        </nav>
                    </div>

                    <div class="header__right--list header__right--biznes">

                        <div class="header__right--title biznes__title">
                            <a href="https://kazakhstan.travel/business">
                                {{ __('frontend.menu4') }}
                            </a>
                        </div>
                        <div class="biznes__list">
                            <ul class="biznes-select">
                                <li><a href="https://eqonaq.kz/">{{ __('frontend.menu5') }}</a></li>
                                <li><a href="https://altynorda.kazakhstan.travel">{{ __('frontend.menu6') }}</a></li>
                                <li><a href="https://kazakhstan.travel/photobank">{{ __('frontend.menu7') }}</a></li>
                                <li><a href="http://safe.kazakhstan.travel/">{{ __('frontend.menu8') }}</a></li>
                            </ul>
                        </div>

                    </div>

                </div>
                <div class="header__right">

                    
                    <!-- <div class="header__right--list header__right--mice">
                        <div class="header__right--title mice__title">
                            MICE
                        </div>
                    </div> -->

                    <div class="header__language">
                        <div class="header__language--title">
                            <?= app()->getLocale() === 'kz' ? 'Қазақ' : '' ?>
                            <?= app()->getLocale() === 'ru' ? 'Русский' : '' ?>
                            <?= app()->getLocale() === 'en' ? 'English' : '' ?>
                        </div>
                        <div class="header__language--list">
                            <ul class="langs-select">

                                <li><a href="<?= route('setlocale', ['lang' => 'kz']) ?>" data-lang="kz">Қазақ</a></li>
                                <li><a href="<?= route('setlocale', ['lang' => 'ru']) ?>" data-lang="ru">Русский</a></li>
                                <li><a href="<?= route('setlocale', ['lang' => 'en']) ?>" data-lang="en">English</a></li>
                                        
                            </ul>
                        </div>
                    </div>
                    <div class="header__search">
                        <a class="header-search" href="https://kazakhstan.travel/search">
                            {{-- <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.3742 9.60158L14.6329 12.8602C15.1224 13.3497 15.1224 14.1434 14.6329 14.6329C14.1433 15.1224 13.3497 15.1224 12.8601 14.6329L9.6014 11.3742C9.59161 11.3644 9.5835 11.3536 9.57539 11.3429C9.56916 11.3346 9.56292 11.3263 9.55591 11.3185C8.58327 11.9597 7.41904 12.3339 6.1669 12.3339C2.76099 12.3339 0 9.57281 0 6.16696C0 2.76105 2.76105 0 6.16696 0C9.57287 0 12.3339 2.76105 12.3339 6.16702C12.3339 7.4191 11.9597 8.58339 11.3185 9.55603C11.3263 9.563 11.3345 9.56921 11.3428 9.57542C11.3536 9.58359 11.3644 9.59174 11.3742 9.60158ZM2.13782 6.16696C2.13782 8.39217 3.94169 10.196 6.16695 10.196C8.39209 10.196 10.196 8.39217 10.196 6.16696C10.196 3.94176 8.39209 2.13783 6.16695 2.13783C3.94175 2.13783 2.13782 3.9417 2.13782 6.16696Z" class="svg_fill" fill="#3379FF"/>
                            </svg> --}}
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                                <path d="M17.2884 15.8509L11.8805 10.4065C11.4875 10.0108 10.8504 10.0108 10.4574 10.4065C10.0644 10.8021 10.0644 11.4436 10.4574 11.8392L15.8653 17.2836C16.2582 17.6792 16.8954 17.6792 17.2884 17.2836C17.6814 16.888 17.6814 16.2465 17.2884 15.8509Z" fill="#67A1D6"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.84285 13.7781C3.06365 13.7781 0 10.6938 0 6.88906C0 3.08434 3.06365 0 6.84285 0C10.622 0 13.6857 3.08434 13.6857 6.88906C13.6857 10.6938 10.622 13.7781 6.84285 13.7781ZM6.84285 11.7446C9.50648 11.7446 11.6658 9.57068 11.6658 6.88906C11.6658 4.20745 9.50648 2.03356 6.84285 2.03356C4.17922 2.03356 2.01992 4.20745 2.01992 6.88906C2.01992 9.57068 4.17922 11.7446 6.84285 11.7446Z" fill="#67A1D6"/>
                            </svg>
                        </a>
                    </div>
                    <div class="header__login">
                        <a class="header-login" href="https://kazakhstan.travel/cabinet">
                            {{-- <svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.7083 8.29163C11.5138 8.09712 11.2778 8.00004 10.9999 8.00004H3.33325V4.66673C3.33325 3.9305 3.59372 3.30211 4.11459 2.78124C4.63549 2.26049 5.26392 2.00005 6.00009 2.00005C6.73619 2.00005 7.36473 2.26045 7.88541 2.78124C8.40632 3.30211 8.66679 3.93053 8.66679 4.66673C8.66679 4.84723 8.73272 5.00346 8.86454 5.13539C8.99665 5.26736 9.15296 5.33336 9.33327 5.33336H10.0003C10.1807 5.33336 10.3369 5.26736 10.469 5.13539C10.6006 5.00346 10.6668 4.84723 10.6668 4.66673C10.6668 3.38187 10.2102 2.28311 9.29707 1.36984C8.38384 0.456541 7.28479 0 6.00009 0C4.71529 0 3.61634 0.456541 2.70303 1.36981C1.78987 2.28296 1.33329 3.38183 1.33329 4.6667V8H0.999997C0.722305 8 0.486134 8.09731 0.291666 8.29159C0.0971976 8.48588 0 8.72212 0 8.99992V15C0 15.2779 0.0972341 15.5141 0.291666 15.7084C0.486134 15.9027 0.722305 16 0.999997 16H10.9999C11.2778 16 11.514 15.9027 11.7083 15.7084C11.9026 15.5141 12 15.2779 12 15V8.99995C12.0001 8.72215 11.9029 8.48617 11.7083 8.29163Z" class="svg_fill" fill="#3379FF"/>
                            </svg> --}}
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16" fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M10 6.6665V4C10 1.7915 8.20874 0 6 0C3.79126 0 2 1.7915 2 4V6.6665H0V16H12V6.6665H10ZM6.66675 11.8154V13.3335H5.3335V11.8154C4.93677 11.584 4.66675 11.1597 4.66675 10.667C4.66675 9.93066 5.26416 9.3335 6 9.3335C6.73608 9.3335 7.3335 9.93066 7.3335 10.667C7.3335 11.1587 7.06396 11.584 6.66675 11.8154ZM3.33325 4V6.66699H8.6665V4C8.6665 2.5293 7.46997 1.3335 6 1.3335C4.5293 1.3335 3.33325 2.5293 3.33325 4Z" fill="#67A1D6"/>
                            </svg>
                        </a>
                    </div>

                    <div class="header-toggle"><i></i></div>

                </div>

            </div>
        </div>

    </header>

    <div class="header-menu">
        <div class="header-toggle"><i></i></div>
        <nav class="nav">
            <a class="nav-link" href="https://kazakhstan.travel/">{{ __('frontend.menu1') }}</a>
            <a class="nav-link" href="https://kazakhstan.travel/">{{ __('frontend.menu2') }}</a>
            <a class="nav-link" href="https://kazakhstan.travel/">{{ __('frontend.menu3') }}</a>
            </nav>
        <nav class="header-links">
            <div class="dropdown">
                <div class="dropdown-current"><input type="text" readonly="" value="{{ __('frontend.menu4') }}"></div>
                <ul class="dropdown-select clearlist">
                    <li><a href="https://eqonaq.kz/">{{ __('frontend.menu5') }}</a></li>
                    <li><a href="https://altynorda.kazakhstan.travel">{{ __('frontend.menu6') }}</a></li>
                    <li><a href="https://kazakhstan.travel/photobank">{{ __('frontend.menu7') }}</a></li>
                    <li><a href="http://safe.kazakhstan.travel/">{{ __('frontend.menu8') }}</a></li>
                </ul>
            </div>
            <!-- <a href="https://kazakhstan.travel/auth">Login</a>
            <form class="header-search-form" action="https://kazakhstan.travel/search">
                <input type="text" class="form-control" name="request" placeholder="Search">
                <input type="submit">
            </form> -->
        </nav>
        <div class="langs">
            <div class="langs-current">
                <input type="text" readonly="" value="
                    <?= app()->getLocale() === 'kz' ? 'Қазақ' : '' ?>
                    <?= app()->getLocale() === 'ru' ? 'Русский' : '' ?>
                    <?= app()->getLocale() === 'en' ? 'English' : '' ?>
                ">
            </div>
            <ul class="clearlist langs-select">
                
                <li class="header__lang--item <?= app()->getLocale() === 'kz' ? 'active"' : '' ?>"><a href="<?= route('setlocale', ['lang' => 'kz']) ?>" data-lang="kz">Қазақ</a></li>
                <li class="header__lang--item <?= app()->getLocale() === 'ru' ? 'active"' : '' ?>"><a href="<?= route('setlocale', ['lang' => 'ru']) ?>" data-lang="ru">Русский</a></li>
                <li class="header__lang--item <?= app()->getLocale() === 'en' ? 'active"' : '' ?>"><a href="<?= route('setlocale', ['lang' => 'en']) ?>" data-lang="en">English</a></li>
                
            </ul>
        </div>
    </div>


    <div class="wrapper">

        @yield('content')

    </div>


    <footer>
        <div class="container">

            <div class="footer__row">

                <div class="footer__left">

                    <div class="footer__left--left">
                        <div class="footer__logo">
                            <a href="https://kazakhstan.travel/">
                                <img src="/img/logo.svg" alt="">
                            </a>
                        </div>
                        <div class="footer__feedback">
                            <a href="https://kazakhstan.travel/feedback">Contact us</a>
                        </div>
                        <div class="footer-socials">
                            <a href="https://www.facebook.com/TraveltoKazakhstan/" target="_blank"><img src="/img/icon-facebook.svg" alt=""></a>
                            <a href="https://twitter.com/qazaqstantravel" target="_blank"><img src="/img/icon-twitter.svg" alt=""></a>
                            <a href="https://www.instagram.com/travel.kazakhstan/" target="_blank"><img src="/img/icon-instagram.svg" alt=""></a>
                            <a href="https://youtube.com/Kazakhstantravel" target="_blank"><img src="/img/icon-youtube.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="footer__left--right">
                        <div class="footer__left--list">

                            <nav class="footer-menu">
                                <a class="footer-menu-link" href="https://kazakhstan.travel/business">{{ __('frontend.menu4') }}</a>
                                <a class="footer-menu-link" href="https://kazakhstan.travel/tourist-help/en/resources">{{ __('frontend.menu10') }}</a>
                                <a class="footer-menu-link" href="https://kazakhstan.travel/partners">{{ __('frontend.menu11') }}</a>
                            </nav>

                            <nav class="footer-menu">
                                <a class="footer-menu-link" href="https://kazakhstan.travel/privacy">{{ __('frontend.menu12') }}</a>
                                <a class="footer-menu-link" href="https://kazakhstan.travel/about">{{ __('frontend.menu13') }}</a>
                                <a class="footer-menu-link" href="https://kazakhstan.travel/contacts">{{ __('frontend.menu14') }}</a>
                                <a class="footer-menu-link" href="https://kazakhstan.travel/feedback">{{ __('frontend.menu15') }}</a>
                            </nav>

                        </div>
                    </div>

                </div>
                <div class="footer__right">

                    <div class="footer-block">
                        <a class="footer-menu-logo-link" target="_blank" href="https://www.gov.kz/memleket/entities/mcs?lang=en">
                            <div class="footer-logo footer-logo--small">
                                <img src="/img/MCS_Logo.png" alt="">
                            </div>
                            <div class="footer-text">{{ __('frontend.menu17') }}</div>
                        </a>
                    </div>
                    <div class="footer-block">
                        <a class="footer-menu-logo-link" href="http://qaztourism.kz/" target="_blank">
                            <div class="footer-logo footer-logo--small">
                                <img src="/img/logo-kt-full.png" alt="">
                            </div>
                            <div class="footer-text">{{ __('frontend.menu18') }}</div>
                        </a>
                    </div>

                </div>

            </div>

        </div>
    </footer>

    <a class="md-trigger" id="march_id" data-modal="success">Подробнее</a>
    <div class="md-modal modal_nopadding md-effect-1" id="success">
        <button class="md-close ">
            <img src="/img/close.svg" alt="">
        </button>
        <div class="md-content">
            <div class="success__block">
                <div class="success--text">
                    {{ __('frontend.success_title') }}
                </div>
                <div class="success--linck">
                    <a href="/">
                        {{ __('frontend.success_linck') }}
                    </a>
                </div>
            </div>

        </div>
    </div>

    <div class="md-overlay"></div>

    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/js/jquery.datepicker.extension.range.min.js"></script>

    {{-- <script src="/js/jquery.maskedinput.min.js"></script> --}}
    <script src="/js/jquery.inputmask.js"></script>

    <script src="/js/classie.js"></script>
    <script src="/js/modalEffects.js"></script>

    <script src="/js/swiper.min.js"></script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

    <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6Ld8K_EaAAAAANHx-FbOQ65H_lwRHXs4tHSEgnEJ'
        });
      };
    </script>

        <?php if(app()->getLocale() === 'kz'){ ?>
            <script>
                /* Локализация datepicker */
                $.datepicker.regional['kz'] = {
                    closeText: 'Жабу',
                    prevText: '&#x3c;Алдыңғы',
                    nextText: 'Келесі&#x3e;',
                    currentText: 'Бүгін',
                    monthNames: ['Қаңтар','Ақпан','Наурыз','Сәуір','Мамыр','Маусым',
                    'Шілде','Тамыз','Қыркүйек','Қазан','Қараша','Желтоқсан'],
                    monthNamesShort: ['Қаң','Ақп','Нау','Сәу','Мам','Мау',
                    'Шіл','Там','Қыр','Қаз','Қар','Жел'],
                    dayNames: ['Жексенбі','Дүйсенбі','Сейсенбі','Сәрсенбі','Бейсенбі','Жұма','Сенбі'],
                    dayNamesShort: ['жкс','дсн','ссн','срс','бсн','жма','снб'],
                    dayNamesMin: ['Жк','Дс','Сс','Ср','Бс','Жм','Сн'],
                    weekHeader: 'Не',
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional['kz']);
            </script>
        <?php } ?>

        <?php if(app()->getLocale() === 'ru'){ ?>
            <script>
                /* Локализация datepicker */
                $.datepicker.regional['ru'] = {
                    closeText: 'Закрыть',
                    prevText: 'Предыдущий',
                    nextText: 'Следующий',
                    currentText: 'Сегодня',
                    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                    monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
                    dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                    dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                    dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                    weekHeader: 'Не',
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional['ru']);
            </script>
        <?php } ?>

    <script src="/js/script.js?v=0.0.3"></script>


</body>

</html>
