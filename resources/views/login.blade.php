@extends('layouts.index')

@section('content')

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="form__block form__block--page">
                        <div class="form__block--content">
                            <form action="{{ route('backend.login') }}" method="POST" id="logint_form">
                                @csrf
                                <div class="form__block--form">
                                    <div class="form__block--subtitle">
                                        Вход
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form__listitem--line">
                                                <div class="form__listitem--title">
                                                   E-mail<span class="form_red">*</span>
                                                </div>
                                                <div class="form__listitem--input">
                                                    <input type="text" class="input-linck" required="" name="email" value="">
                                                    <span class="input-required">Неверное имя пользователя или пароль!</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form__listitem--line">
                                                <div class="form__listitem--title">
                                                    Пароль<span class="form_red">*</span>
                                                </div>
                                                <div class="form__listitem--input">
                                                    <input type="password" class="input-linck" required="" name="password" value="">
                                                    <span class="input-required">Неверное имя пользователя или пароль!</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form__listitem--line">
                                                <div class="form__listitem--input">
                                                    <button class="input__button">Войти</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
