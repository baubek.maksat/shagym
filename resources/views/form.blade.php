@extends('layouts.index')

@section('content')

    <div class="content">

        <div class="container">

            <div class="form__block form__block--page">

                <div class="form__block--content">

                    <div class="form__prev">
                        <a href="/{{ app()->getLocale() }}">
                            <svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.08757 12.496L7.69865 11.8799C7.89953 11.6664 8 11.4171 8 11.1324C8 10.842 7.89953 10.5956 7.69865 10.3929L3.73923 6.40003L7.69854 2.40718C7.89942 2.20454 7.99989 1.95811 7.99989 1.66773C7.99989 1.38297 7.89942 1.13368 7.69854 0.920164L7.08746 0.312189C6.88118 0.104025 6.634 -4.70714e-08 6.34612 -5.69915e-08C6.05287 -6.70964e-08 5.80848 0.10414 5.61299 0.31216L0.309536 5.66055C0.103235 5.85781 -3.38462e-07 6.10416 -3.54866e-07 6.4C-3.70962e-07 6.69029 0.103207 6.93955 0.309536 7.14751L5.61299 12.4959C5.81399 12.6986 6.05835 12.8 6.34612 12.8C6.62863 12.8 6.87568 12.6986 7.08757 12.496Z" fill="#8C8C8C"/>
                            </svg>
                            {{ __('frontend.prev') }}
                          
                        </a>
                    </div>

                    <div class="form__block--form">
                        <form id="form__list" enctype="multipart/form-data" method="post">
                            @csrf
                            @include('forms.s'.$view_id)
                            @include('forms.s9')
                        </form>
                    </div>

                </div>


                <div class="form__block--next">
                    <a class="form__next">
                    {{ __('frontend.next') }}
                        
                    </a>
                </div>

            </div>

        </div>


    </div>
@endsection
