<div class="form__block--title">
{{ __('frontend.Restaurant, café, etc') }}
   
</div>

<div class="form__listitem form__listitem--s5 form__listitem--active" data-spage="s5">

    <div class="form__listitem--line">

        <div class="form__listitem--title">
        {{ __('frontend.s_5_What is the name of the restaurant/café you’re complaining about?') }}
            <span class="form_red">*</span>
        </div>
        <div class="form__listitem--input">
            <input type="text" class="input-linck" required="" name="name" value="">
            <span class="input-required">{{ __('frontend.s_1_required field') }}</span>
        </div>

    </div>
    <div class="form__listitem--line">

        <div class="row">
            <div class="col-lg-8">
                <div class="form__listitem--title">
                {{ __('frontend.s_5_Where is the restaurant located?') }}
                    <span class="form_red">*</span>
                </div>
                @include('inc.region')
            </div>
            <div class="col-lg-4">
                <div class="form__listitem--title">
                {{ __('frontend.s_5_Date of visit') }}
                    
                </div>
                <div class="form__listitem--input">
                    <div class="date_range--relative">
                        <input type="text" class="input-linck" id="date_range">
                        <input type="hidden" name="date_start" id="date_start" value="">
                        <input type="hidden" name="date_end" id="date_end" value="">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="form__listitem--line">

        <div class="form__listitem--title">
        {{ __('frontend.s_2_What are you complaining about?') }}
        </div>
        <div class="form__listitem--input">
            
            @include('inc.negative')
            
        </div>

    </div>
    <div class="form__listitem--line">

        <div class="form__listitem--title">
        {{ __('frontend.s_2_Please give details of complaint') }}<span class="form_red">*</span>
        </div>
        <div class="form__listitem--input">
            <textarea class="input-linck input-textarea" required="" name="comment_text"></textarea>
            <span class="input-required">{{ __('frontend.s_1_required field') }}</span>
        </div>

    </div>
    <div class="form__listitem--line">

        <div class="form__listitem--title">
        {{ __('frontend.s_1_Upload photos') }}
        </div>
        <div class="form__listitem--input">
            <ul id="attached-fiels-list"></ul>
            <label class="file__input">
                <input type="file" class="input-file" multiple name="photos[]" id="comment-files" accept="image/*,image/jpeg">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14 2H6C5.46957 2 4.96086 2.21071 4.58579 2.58579C4.21071 2.96086 4 3.46957 4 4V20C4 20.5304 4.21071 21.0391 4.58579 21.4142C4.96086 21.7893 5.46957 22 6 22H18C18.5304 22 19.0391 21.7893 19.4142 21.4142C19.7893 21.0391 20 20.5304 20 20V8L14 2Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M14 2V8H20" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M12 18V12" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M9 15H15" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                {{ __('frontend.s_1_btn_foto') }}
            </label>
        </div>

    </div>

</div>
