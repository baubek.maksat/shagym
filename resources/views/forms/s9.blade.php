<div class="form__listitem form__listitem--s9" data-spage="s9">

    <div class="form__block--subtitle">
    {{ __('frontend.s_9_name0') }}
    </div>

    <div class="row">
        <div class="col-lg-4 offset-lg-4">
            <div class="form__listitem--line">
                <div class="form__listitem--title">
                {{ __('frontend.s_9_name') }}
                    <span class="form_red">*</span>
                </div>
                <div class="form__listitem--input">
                    <input type="text" class="input-linck" required="" name="client_name" value="">
                    <span class="input-required">{{ __('frontend.s_1_required field') }}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 offset-lg-4">
            <div class="form__listitem--line">
                <div class="form__listitem--title">
                    Email<span class="form_red">*</span>
                </div>
                <div class="form__listitem--input">
                    <input type="text" class="input-linck" required="" name="client_email" value="">
                    <span class="input-required">{{ __('frontend.s_1_required field') }}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4 offset-lg-4">
            <div class="form__listitem--line">
                <div class="form__listitem--title">
                {{ __('frontend.s_9_Phone number') }}<span class="form_red">*</span>
                </div>
                <div class="form__listitem--input">
                    <input type="text" class="input-linck" required="" name="client_phone" value="">
                    <span class="input-required">{{ __('frontend.s_1_required field') }}</span>
                </div>
            </div>
        </div>

        <div class="col-lg-4 offset-lg-4">
            <div class="form__listitem--input">
                <div class="g-recaptcha" id="html_element"></div>
                <span class="input-required" id="recaptchaError">{{ __('frontend.s_1_required field') }}</span>
            </div>
            <br>
        </div>

        <input type="hidden" name="view_id" value="{{ $view_id }}">
        <div class="col-lg-4 offset-lg-4">
            <div class="form__listitem--line">
                <div class="form__listitem--input">
                    <button class="input__button input__button-click"> {{ __('frontend.s_9_sub') }}</button>
                </div>
            </div>
        </div>
    </div>



</div>
